//通过UITableViewDelegate方法可以实现删除 tableview中某一行 
//滑动删除
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{   
    NSUInteger row = [indexPath row];
    [bookInforemoveObjectAtIndex:row];//bookInfo为当前table中显示的array
    [tableView deleteRowsAtIndexPaths:[NSArrayarrayWithObject:indexPath]withRowAnimation:UITableViewRowAnimationLeft];
}
 
/*此时删除按钮为Delete，如果想显示为“删除” 中文的话，则需要实现
UITableViewDelegate中的- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath方法*/
 
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath{    
    return @"删除";    
}    
//或者，最简单的方式，将plist中的Localization native development region改为China即可
 
//这只是让默认的Delete按钮显示成了中文的删除按钮而已，如果想将这个删除按钮换成其他图片形式的，怎么办呢？
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath    
{    
    static NSString *RootViewControllerCell = @"RootViewControllerCell";    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:RootViewControllerCell];    
    if(cell == nil)    
    {    
        cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:RootViewControllerCell]autorelease];    
             
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];    
        [button setBackgroundImage:[UIImage imageNamed:@"delete.png"] forState:UIControlStateNormal];    
        [button setFrame:CGRectMake(280, 10, 30, 30)];    
        [button addTarget:self action:@selector(del:) forControlEvents:UIControlEventTouchUpInside];    
        [cell.contentView addSubview:button];           
    }    
         
    cell.textLabel.text = [array objectAtIndex:[indexPath row]];    
    cell.tag = [indexPath row];    
         
    NSArray *subviews = [cell.contentView subviews];    
    for(id view in subviews)    
    {    
        if([view isKindOfClass:[UIButton class]])    
        {    
            [view setTag:[indexPath row]];    
            [cell.contentView bringSubviewToFront:view];    
        }    
    }    
    return cell;    
}    
     
-(void)del:(UIButton *)button    
{    
    NSArray *visiblecells = [self.table visibleCells];    
    for(UITableViewCell *cell in visiblecells)    
    {    
        if(cell.tag == button.tag)    
        {    
            [array removeObjectAtIndex:[cell tag]];    
            [table reloadData];    
            break;    
        }    
    }    
}